import { Colour } from './Colour/Colour';

export function getRandomArray(len: number, avaibleColours: Colour[]): Colour[] {
    if(avaibleColours.length < 1) return [];
    const table: Colour[] = [];
    Array.from(Array(len)).forEach(
        () => Array.from(Array(len)).forEach(
            () =>
                table.push(getRandomColor(avaibleColours))
        ));
    return table;
}

function getRandomColor(avaibleColours: Colour[]): Colour {
    const index = Math.floor(Math.random() * avaibleColours.length);
    const randomColour = avaibleColours[index];
    if (randomColour) {
        return randomColour;
    } else {
       throw new Error('getRandomColor() in dataGenerator.tsx failed. I cannot return null');
    }
}