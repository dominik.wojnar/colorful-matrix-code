import React from 'react';
import { ColourPicker } from './Colour/ColourPicker';

export class Custom2DBarCode extends React.Component {
    render() {
        const length = 10;
        return (
            <React.Fragment>
                <ColourPicker length={length}/>
            </React.Fragment>
        );
    }
}
