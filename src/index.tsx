import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import { Custom2DBarCode } from './custom2DBarCode';

ReactDOM.render(<Custom2DBarCode />, document.getElementById('root'));
//ReactDOM.render(<ColourPicker />, document.getElementById('root'));


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
