export interface Compressor {
    compressString(data: string):BigInt;
    compressBinaryData(data: BigInt):BigInt;
}

export class BaseCompressor implements Compressor{
    compressBinaryData(data: BigInt): BigInt {
        return BigInt(0);
    }

    compressString(data: string): BigInt {
        return BigInt(0);
    }

}
