import { Colour } from './Colour';
export const SAFE_COLOURS = [
    Colour.newColour(0,0,0), Colour.newColour(255,255,255),
    Colour.newColour(255,0,0),Colour.newColour(0,255,0), Colour.newColour(0,0,255),
    Colour.newColour(255,255,0), Colour.newColour(255,0,255), Colour.newColour(0,255,255)
];
