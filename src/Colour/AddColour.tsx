import React, { ChangeEvent } from "react";
import { Colour } from "./Colour";

interface IAddColourProps {
    onChange: (event: Colour) => void;
}
interface IAddColourState {
    colourToAdd: Colour
}

export class AddColour extends React.Component<IAddColourProps, IAddColourState> {
    constructor(props: IAddColourProps) {
        super(props);
        this.state = {
            colourToAdd: Colour.newColour(66, 66, 66)
        };
        this.onChangeHandler = this.onChangeHandler.bind(this);
        this.onClickHandler = this.onClickHandler.bind(this);
    }

    public static stringToColour(hex: string): Colour {
        return Colour.newColour(
            parseInt(hex.slice(1, 3), 16), //red
            parseInt(hex.slice(3, 5), 16), //green
            parseInt(hex.slice(5, 7), 16)  //blue
        )
    }

    onChangeHandler(event: ChangeEvent<HTMLInputElement>) {
        console.log(event.target.value);
        const newColour = AddColour.stringToColour(event.target.value);
        this.setState({
            colourToAdd: newColour
        });
        
    }
    onClickHandler(event: any) {
        event.preventDefault();
        this.props.onChange(this.state.colourToAdd);
    }
    render() {
        return (
            <form>
                <input type="color" name="addColour" value={this.state.colourToAdd.name} onChange={this.onChangeHandler} />
                <input type="submit" name="Submit" onClick={this.onClickHandler} />
            </form>
        )
    }
}
